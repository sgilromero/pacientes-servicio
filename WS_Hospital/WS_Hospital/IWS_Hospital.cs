﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WS_Hospital
{
    [ServiceContract]
    public interface IWS_Hospital
    {
        [OperationContract]
        List<CNPaciente> ObtenerPacientes();
    }    
}
