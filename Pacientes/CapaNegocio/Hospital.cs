﻿using CapaServicio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CapaNegocio
{
    public class Hospital
    {
        public static List<CNPaciente> ObtenerPacientes()
        {
            var respuesta = new List<CNPaciente>();

            try
            {
                var servicio = Servicio.ObtenerPacientes();

                respuesta = servicio.Select(x => new CNPaciente {
                    ID = x.ID,
                    Apellido = x.Apellido,
                    Nombre = x.Nombre,
                    Edad = x.Edad,
                    Domicilio = x.Domicilio
                }).ToList();

                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }
}
