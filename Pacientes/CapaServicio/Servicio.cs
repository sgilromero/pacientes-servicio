﻿using CapaServicio.WS_Hospital;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CapaServicio
{
    public class Servicio
    {
        public static List<CNPaciente> ObtenerPacientes()
        {
            try
            {
                WS_HospitalClient _servicio = new WS_HospitalClient();

                var respuesta = _servicio.ObtenerPacientes().ToList();

                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
